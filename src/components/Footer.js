import {Navbar, Container} from "react-bootstrap";

export default function Footer(){

	return(
		<Navbar bg="dark" className="container-fluid">
			<Navbar  className="p-3 flex light">
		          <div className="container-fluid text-light">
		            <p className="text-center">&copy; Copyright <strong>William Louis L. Alambatin</strong>. All Rights Reserved 2022</p>
		          </div>
			</Navbar>
		</Navbar>


	)
}