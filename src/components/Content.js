import {useEffect, useState, useContext} from "react";
import ProductCard from "../components/ProductCard";
import {Container, Row} from "react-bootstrap";
import {Navigate} from "react-router-dom";
import UserContext from "../UserContext";

export default function Content(){

	const [products, setProducts]= useState([]);

	// To be used for validating the "role" of the user.
	const {user} = useContext(UserContext);

	//Retrieve the products from the database upon initial render of the product component.
	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/products`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProducts(data.map(product => {
				return(
					<ProductCard key={product._id} productProp={product}/>
					)
			}))
		})
	},[])

	return(
			(user.isAdmin)
				? // it means admin is true
					<Navigate to="/admin"/>
				:
					<>
						<Container>	
							<h1 className="text-center mt-5">List of all Products</h1>
							<Row className="mt-3 mb-3">
										{products}
							</Row>
						</Container>	
					</>

		)
}