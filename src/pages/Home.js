import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
import Footer from "../components/Footer";

export default function Home(){
	const data = {
		title: "Digital Shop Online",
		content: "Your One Stop Digital Shop.",
		destination: "/products",
		label: "Order Now"
	}

	return(
		<>
			<Banner data={data}/>
        	{<Highlights />}

        	<Footer/>
		</>
	)
}